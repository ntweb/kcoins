//import { Express } from "express";
import express  from "express";
import { Routes } from './server/routes'
import bodyParser from 'body-parser'

class Server {
    public app : express.Application;
    public routes : Routes = new Routes;

    constructor() {
        this.app = express();
        // config reading json from body with body-parser help
        //this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        this.routes.routes(this.app);

        // config reading json from body with body-parser help
        //this.app.use(bodyParser.urlencoded({ extended: true }));
        //this.app.use(bodyParser.json());
    }
}

export default new Server().app;