import Transaction from './transaction'

export default class Block {

    prevHash : string;
    transactions : Transaction[];
    index: number;
    hash: string;
    proof : number; // nonce
    constructor (index : number, prevHash : string, hash: string, transactions : Transaction[], proof: number) {
       this.index = index;
       this.prevHash = prevHash;
       this.hash = hash;
       this.transactions = transactions;
       this.proof = proof;
    }



    /* Convert the block to string( for hashin ), I use string instead of json, because  
        if I want to use another programming language like c++ or python, json output is not
        standard (exactlly same)
    */
   toStr() : string 
   {
    let str  = "index=" + this.index;
    str+= ", hash=" + this.hash;
    str+= ", prevHash=" + this.prevHash;
    str+= ", proof=" + this.proof;
    str+= ", transactions=";
    this.transactions.forEach ( tx => {
        str += '{' + tx.toStr() + '},'
    });
      
    return str;
   }
}