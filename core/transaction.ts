export default class Transaction {
    amount : number; // in javascript numbers are floating point value
    sender : string;
    recipient : string;
    id : string

    constructor(id : string, sender:string , recipient: string, amount : number){
        this.id = id;
        this.sender = sender;
        this.recipient = recipient;
        this.amount = amount;
    }


    /* Convert the transaction to string( for hashin ), I use string instead of json, because  
        if I want to use another programming language like c++ or python, json output is not
        standard (exactlly same)
    */
   toStr() : string 
   {
    let str  = "amount=" + this.amount;
    str+= ", sender=" + this.sender;
    str+= ", recipient=" + this.recipient;
    str+= ", id=" + this.id;
    return str;
   }
}