//import { v1 as uuid } from 'uuid';
import  Block  from './block';
import { sha256 } from 'js-sha256'
import Transaction from './transaction'
import path from 'path'
import fs from 'fs'
export default class Blockchain {

    private chain : Block[] = []; 
    private bendingTransactions : Transaction[] = [];

    // path for chain file
    private p = path.join( path.dirname(__dirname), 'data', 'kcoins.json' );


    constructor() {
        this.loadChain();
        // add genesis block, Hint: ( first block in the blockchain called genesis )
        
    }

    // load blockchain from file
    loadChain() {
        
        fs.readFile(this.p, (err, content) => {
            // file note found
            if (err) {
                console.log("Error load kcoins chain");
            }
        });
    }

    // mining new block block
    createNewBlock(proof: number , prevHash: string , hash: string) {
        const block = new Block(this.chain.length+1, prevHash, hash, this.bendingTransactions, proof);
        
        // empty current open transaction ofter mining 
        this.bendingTransactions = [];

        // add currently mined block to our chain
        this.chain.push(block);
    }
        


    // add new transaction to out bendingTransaction
    createNewTransaction(tx : Transaction) {
        //tx.id = uuid().split('-').join('');
        tx.id = "0x";
        this.bendingTransactions.push(tx);
        return tx;
    }

   getLastBlock() {
       return this.chain[this.chain.length-1];
   }

   getCurrentData() : Block {
       const block = new Block(this.chain.length+1, this.getLastBlock().hash, '',this.bendingTransactions, 0);
        return block;
   }
   getChain() {
       return this.chain;
   }


   // hash a block using sha256
   hashBlock(prevHash: string, currentBlock : Block, proof : number) {
       const dataAsString = prevHash + proof.toString() + currentBlock.toStr()
       const hash = sha256(dataAsString);
       //console.log(dataAsString);
       return hash;
   }


   profOfWork(prevHash : string, currentBlock : Block) {
       let proof = 0;
       let hash = this.hashBlock(prevHash, currentBlock, proof);
       while (hash.substring(0,5) !== '00000') {
           proof++;
           hash = this.hashBlock(prevHash, currentBlock, proof); 
       }
       console.log(proof);
       return proof;
   }
}