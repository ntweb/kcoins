import {Request, Response, Express} from "express";
import { TransactionController } from './controllers/transaction'

export class Routes {
    txConntroller = new TransactionController();

    public routes(app : any) : void {
        app.route('/').get(this.txConntroller.getMainPage);
        app.route('/transaction/add').post(this.txConntroller.add);
    }
}
